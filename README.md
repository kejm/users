# Users
Its simple Spring Boot Rest application with database, 
which can by use as basic fundamental for other app which need users service.

# Installation
1. In first you need Oracle MySQL (yes this with dolphin ;])
2. Second step is change application.properties:
You must change "validate" in spring.jpa.hibernate.ddl-auto and set "create"
Create is necessary ony when you want to build first time your database. Next time you can run program with "validate"

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
Please make sure to update tests as appropriate.

## License
