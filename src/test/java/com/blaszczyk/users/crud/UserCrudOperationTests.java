package com.blaszczyk.users.crud;

import com.blaszczyk.users.entities.User;
import com.blaszczyk.users.repositories.UserRepository;
import com.blaszczyk.users.services.UserService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.hamcrest.Matchers.hasItems;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
public class UserCrudOperationTests {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    @Autowired
    private UserService userServiceAutowired;

    @Test
    public void mockAddUserTest() {
        User userTest = mock(User.class);

        when(userRepository.save(userTest)).thenReturn(userTest);
        userRepository.save(userTest);

        userService.deleteUser(userTest.getId());
        assertNull(userService.getUserById(userTest.getId()));
    }

    @Test
    public void getUserByIdTest() {
        User exceptUser = new User(1l, "Krystian", "another", userServiceAutowired.getUserById(1l).getRole());
        when(userRepository.getUserById(1l)).thenReturn(exceptUser);
        User actualUser = userServiceAutowired.getUserById(1l);
        assertEquals(exceptUser.getName(), actualUser.getName());
    }

    @Test
    public void getAllUsersTest() {
        List<User> actualList = userServiceAutowired.getAllUsers();
        User expectUser = userServiceAutowired.getUserById(1l);
        User expectUser2 = userServiceAutowired.getUserById(2l);

        when(userRepository.findAll()).thenReturn(actualList);

        assertThat(actualList, hasItems(
                expectUser,
                expectUser2
        ));
    }

    @Test
    public void addUserTest() {
        //Given
        User newUserTest = new User("exampleName", "exampleSurname");
        userServiceAutowired.addUser(newUserTest);
        //When
        when(userRepository.save(newUserTest)).thenReturn(newUserTest);
        User actualUser = userServiceAutowired.getUserById(newUserTest.getId());
        //Then
        assertEquals(newUserTest, actualUser);
        //Clean
        userServiceAutowired.deleteUser(newUserTest.getId());
    }

    @Test
    public void deleteUserTest() {
        User userTest = userServiceAutowired.addUser(new User("exampleNew", "exampleSecond"));
        when(userRepository.save(userTest)).thenReturn(userTest);

        userServiceAutowired.deleteUser(userTest.getId());

        if(userServiceAutowired.getUserById(userTest.getId()) == null){
            System.out.println("User no longer exists");
        }

        assertNull(userServiceAutowired.getUserById(userTest.getId()));
    }

}
