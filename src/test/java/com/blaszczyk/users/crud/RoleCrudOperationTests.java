package com.blaszczyk.users.crud;

import com.blaszczyk.users.entities.Role;
import com.blaszczyk.users.repositories.RoleRepository;
import com.blaszczyk.users.services.RoleService;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
public class RoleCrudOperationTests {

    @Mock
    private RoleRepository roleRepository;

    @InjectMocks
    private RoleService roleService;

    @Autowired
    private RoleService roleServiceAutowired;

    @Before
    void setUp() {
    }

    @After
    void tearDown() {

    }

    @Test
    public void showRoleByIdTest() {
        Role exceptionRole = new Role(1l, "user", "standard");

        when(roleRepository.getById(1l)).thenReturn(exceptionRole);
        Role actualRole = roleServiceAutowired.showRoleById(1l);

        assertEquals(exceptionRole, actualRole);
    }

    @Test
    public void getAllRolesTest() {
        List<Role> actualListRoles = roleServiceAutowired.getAllRoles();

        Role roleFromList = actualListRoles.get(1);
        Role actualRole = roleServiceAutowired.showRoleById(roleFromList.getId());

        when(roleRepository.findAll()).thenReturn(actualListRoles);

        assertEquals(roleFromList, actualRole);
    }

    @Test
    public void addAndDeleteRoleTest() {
        Role roleTest = new Role("test", "test example");

        when(roleRepository.save(roleTest)).thenReturn(roleTest);

        roleServiceAutowired.addRole(roleTest);
        roleServiceAutowired.deleteRole(roleTest.getId());
        roleServiceAutowired.showRoleById(24l);
    }

}
