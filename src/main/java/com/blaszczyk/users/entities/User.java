package com.blaszczyk.users.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String surname;

    @ManyToOne
    @JoinColumn(name = "role_id")
    private Role role;

    public User(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

}
