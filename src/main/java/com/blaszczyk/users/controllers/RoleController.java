package com.blaszczyk.users.controllers;

import com.blaszczyk.users.entities.Role;
import com.blaszczyk.users.services.RoleService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
public class RoleController implements IRoleController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RoleController.class);

    private RoleService roleService;

    @Autowired
    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    @Override
    public Role showRoleById(Long id) {
        return roleService.showRoleById(id);
    }

    @Override
    public List<Role> getAllRoles() {
        return roleService.getAllRoles();
    }

    @Override
    public ResponseEntity<String> addRole(Role role) {
        roleService.addRole(role);
        return ResponseEntity.status(HttpStatus.CREATED).body("Role has been added");
    }

    @Override
    public ResponseEntity<String> deleteRole(Long id) {
        try {
            roleService.deleteRole(id);
            LOGGER.info("Role has been removed");
            return ResponseEntity.status(HttpStatus.OK).body("Role has been removed");
        } catch (EmptyResultDataAccessException e) {
            LOGGER.error("Role with this id doesn't exist");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Role can't be delete");
        }
    }
}
