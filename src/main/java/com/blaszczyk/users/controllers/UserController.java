package com.blaszczyk.users.controllers;

import com.blaszczyk.users.entities.User;
import com.blaszczyk.users.services.UserService;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.NestedServletException;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

//@Slf4j
@RestController
public class UserController implements IUserController{

    protected final Logger log = LoggerFactory.getLogger(getClass());

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @Override
    public User getUserById(Long id) {
        log.trace("its trace");
        log.debug("its debug");
        log.info("its info");
        log.warn("its warn");
        log.error("its error");
        return userService.getUserById(id);
    }

    @Override
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @Override
    public ResponseEntity<String> addUser(User user) {
        try {
            userService.addUser(user);
            log.info("User has been created!");
            return ResponseEntity.status(HttpStatus.CREATED).body("User has been created!");
        } catch (HttpMessageNotReadableException e) {
            log.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Request body problem! Check JSON");
        }
    }

    @Override
    public ResponseEntity<String> deleteUser(Long id) {
        try{
            userService.deleteUser(id);
            log.info("User has been removed");
            return ResponseEntity.status(HttpStatus.OK).body("User has been removed!");
        } catch (EmptyResultDataAccessException e) {
            log.error("User with this id doesn't exist");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("We can delete non existing user!");
        }
    }
    
    @Override
    public Optional<User> searchUser(String name, String surname) {
        return userService.searchUser(name, surname);
    }

}
