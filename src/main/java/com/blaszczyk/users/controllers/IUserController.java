package com.blaszczyk.users.controllers;

import com.blaszczyk.users.entities.User;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping(path = "/user")
public interface IUserController {

    @GetMapping(path = "/show-one")
    public User getUserById(@RequestParam Long id);

    @GetMapping(path = "/show-all")
    public List<User> getAllUsers();

    @PostMapping(path = "/add")
    public ResponseEntity<String> addUser(@RequestBody User user);

    @DeleteMapping(path = "/delete")
    public ResponseEntity<String> deleteUser(@RequestParam Long id);

    @RequestMapping(method = RequestMethod.GET, path = "/search-user")
    public Optional<User> searchUser(@RequestParam(required = false) String name, @RequestParam(required = false) String surname);
}
