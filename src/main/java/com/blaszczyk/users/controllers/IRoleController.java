package com.blaszczyk.users.controllers;

import com.blaszczyk.users.entities.Role;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping(path = "/role")
public interface IRoleController {

    @GetMapping(path = "/get-one")
    public Role showRoleById(Long id);

    @GetMapping(path = "/get-all")
    public List<Role> getAllRoles();

    @PostMapping(path = "/add")
    public ResponseEntity<String> addRole(@RequestBody Role role);

    @DeleteMapping(path = "/delete")
    public ResponseEntity<String> deleteRole(@RequestParam Long id);
}
