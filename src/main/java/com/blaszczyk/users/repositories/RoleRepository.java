package com.blaszczyk.users.repositories;

import com.blaszczyk.users.entities.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository extends CrudRepository<Role, Long> {

    Role getById(Long id);
    List<Role> findAll();
    void deleteById(Long id);

}
