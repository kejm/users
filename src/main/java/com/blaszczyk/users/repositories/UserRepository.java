package com.blaszczyk.users.repositories;

import com.blaszczyk.users.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User getUserById(Long id);
    List<User> findAll();
    void deleteById(Long id);
    Optional<User> findByNameAndSurname(String name, String surname);

}
